package com.brh.crmmovies

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_movie_list.view.*

class MovieFragment : Fragment(), RequestStateListener {

    lateinit var tvEmpty : TextView
    lateinit var recycler : RecyclerView
    lateinit var searchBar : EditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_movie_list, container, false)

        tvEmpty = view.findViewById(R.id.tv_empty)
        recycler = view.findViewById(R.id.list)
        searchBar = view.findViewById(R.id.edit_search)
        searchBar.setOnEditorActionListener{ view, actionId, keyEvent->
            if ((actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEARCH) ||
                    (keyEvent.keyCode == KeyEvent.KEYCODE_ENTER)){
                MovieApp.pseudoSearchVM.searchFor(view.text.toString())
                true
            } else
                false
        }
        with(recycler) {
            layoutManager =  LinearLayoutManager(context)
            adapter = MovieRecyclerViewAdapter(emptyList())
        }

        //if somehow arguments is null, crash I want to know about it
        arguments!!.let {
            if (it.getBoolean(ARG_ISFAVE_VIEW, false)) {
                tvEmpty.visibility = View.VISIBLE
                recycler.visibility = View.GONE
                view.search_bar.visibility = View.GONE
                MovieApp.pseudoFavoriteVM.requestStateListener = this
                MovieApp.pseudoFavoriteVM.getAll()
            } else {
                MovieApp.pseudoSearchVM.requestStateListener = this
            }
        }
        return view
    }

    override fun onStateUpdate(state: RequestState) {
        recycler.post {
            view?.progress_bar?.visibility = View.GONE
            when(state){
                is RequestState.Success-> {
                    if (state.list.isEmpty()) {
                        Snackbar.make(recycler, R.string.no_match, Snackbar.LENGTH_LONG).show()
                        return@post
                    }
                    recycler.visibility = View.VISIBLE
                    (recycler.adapter as MovieRecyclerViewAdapter).updateList(state.list)
                }
                is RequestState.Loading -> {
                    view?.progress_bar?.visibility = View.VISIBLE
                }
            }
        }
    }

    companion object {
        private const val ARG_ISFAVE_VIEW = "argView"

        fun favoritesView() = createFragment(true)

        fun allView() =  createFragment()

         private fun createFragment(favoritesView : Boolean = false) =
            MovieFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(ARG_ISFAVE_VIEW, favoritesView)
                }
            }
    }
}
