package com.brh.crmmovies.model

import androidx.room.*

@Entity
open class Movie(@PrimaryKey val id : String, val title : String, var director : String?, val year : String, var plot : String?, val poster : String, var favorited : Boolean = false)

@Dao
abstract  interface MovieDao {
    @Query("Select * from Movie")
    fun getAll() : List<Movie>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movies: List<Movie>)
    @Delete
    fun deleteAll(movies : List<Movie>)
    @Query("Update Movie SET favorited = :yes Where id IN (:ids)" )
    fun update(yes : Boolean, ids : List<String>)

}

@Entity
class FavoriteMovie(id : String, title : String, director : String?, year : String, plot : String?, poster : String) : Movie(id, title, director, year, plot, poster) {
    constructor(movie : Movie) : this(movie.id, movie.title, movie.director, movie.year, movie.plot, movie.poster)
}

@Dao
 interface FavoriteMovieDao {
    @Query("Select * from FavoriteMovie")
    fun getAll() : List<FavoriteMovie>
    @Query("Select * from FavoriteMovie Where id == :id")
    fun getById(id : String) : FavoriteMovie
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateWithPlotDirector(movie : Movie)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: FavoriteMovie)
    @Delete
    fun remove(movie : FavoriteMovie)
}
