package com.brh.crmmovies

import com.brh.crmmovies.model.Movie

sealed class RequestState {
    class Idle() : RequestState()
    class Loading() : RequestState()
    class FatalError() : RequestState()
    class Error(val error : String, list : List<Movie>) : RequestState()
    class Success(val list : List<Movie>) : RequestState()
}

interface RequestStateListener {
    fun onStateUpdate(state : RequestState)
}