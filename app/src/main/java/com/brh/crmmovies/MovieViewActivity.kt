package com.brh.crmmovies

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_view.*
import androidx.core.app.ActivityCompat.startPostponedEnterTransition
import android.view.ViewTreeObserver
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.graphics.drawable.toDrawable
import com.brh.crmmovies.model.FavoriteMovie


class MovieViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_view)
        postponeEnterTransition();
        var toolbar : Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = MovieApp.selectedMovie.title
        }

        with(MovieApp.selectedMovie) {
            val imageView = findViewById<ImageView>(R.id.iv)
            Picasso.get()
                    .load(poster)
                    .into(imageView)
            startPostponedEnterTransition()
            tv_title.setText("$title - $year")
            imageView.getViewTreeObserver().addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    // Tell the framework to start.
                    imageView.getViewTreeObserver().removeOnPreDrawListener(this)
                    startPostponedEnterTransition()
                    return true
                }
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        println("onCreateOptionsMenu")
        menuInflater.inflate(R.menu.movie, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        var fav = menu.findItem(R.id.favorite)
        var unfav = menu.findItem(R.id.unfavorite)
        if (MovieApp.selectedMovie.favorited){
            unfav.setVisible(true)
            fav.setVisible(false)
        } else {
            fav.setVisible(true)
            unfav.setVisible(false)
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home ->onBackPressed()
            R.id.favorite -> {
                MovieApp.pseudoFavoriteVM.addToFavorties(MovieApp.selectedMovie)
                MovieApp.selectedMovie.favorited = true
                invalidateOptionsMenu()
            }
            R.id.unfavorite -> {
                MovieApp.pseudoFavoriteVM.removeFavorite(FavoriteMovie( MovieApp.selectedMovie))
                MovieApp.selectedMovie.favorited = false
                invalidateOptionsMenu()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        supportFinishAfterTransition();
        super.onBackPressed()
    }

}