package com.brh.crmmovies

import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.brh.crmmovies.model.Movie
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject

class SearchVM() : BaseVM() {
    private var searchRequestQueue = Volley.newRequestQueue(MovieApp.instance)
    private var outstandingVRequest : Request<JSONObject>? = null
    /*
    Not securing key as it is seen in plaintext since omdbapi only supports http.. To secure the key store key on a server
    app will fetch that key and the encrypt the key using Android Keystore and save secured key in preferences or the likes.
    */
    private val url = "http://www.omdbapi.com/?apikey=7b8780bc&s="
    private val urlById = "http://www.omdbapi.com/?apikey=7b8780bc&i="

    init {
        searchRequestQueue.start()
    }

    fun searchFor(text : String) : SearchVM {
        requestState = RequestState.Loading()
        outstandingVRequest?.cancel()
        outstandingVRequest = JsonObjectRequest( url+text, null,
            Response.Listener<JSONObject> {response ->
                    if (response.getBoolean("Response")) {
                        GlobalScope.launch {
                            with(MovieApp.movieDb.movieDao()){
                                deleteAll(getAll()) //clear records
                            }
                            parseAndSaveMovies(response)
                        }
                    } else {
                        requestState = RequestState.Success(emptyList()) //temp have to fetch director and plot
                    }
            },
            Response.ErrorListener {
                requestState = RequestState.FatalError()
            }
        )
        searchRequestQueue.add(outstandingVRequest)
        return this
    }

    private fun parseAndSaveMovies(response: JSONObject) {
        val jsonArray = response.getJSONArray("Search")
        val movieResults = mutableListOf<Movie>()
        for (idx in 0 until jsonArray.length()) {
            val movieJson = jsonArray.getJSONObject(idx)
            with(movieJson) {
                val movie = Movie(
                    getString("imdbID"),
                    getString("Title"),
                    null,
                    getString("Year"),
                    null,
                    getString("Poster")
                )
                var temp = MovieApp.movieDb.favoriteDao().getById(movie.id)
                movie.favorited = if (temp!=null) true else false
                movieResults.add(movie)
            }
        }
        MovieApp.movieDb.movieDao().insert(movieResults)
        requestState = RequestState.Success(movieResults) //temp have to fetch director and plot
    }

}
