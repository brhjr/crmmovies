package com.brh.crmmovies

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.room.Room
import com.brh.crmmovies.model.Movie
import com.brh.crmmovies.model.MovieDb

class MovieApp  : Application() {

    override fun onCreate() {
        super.onCreate()

        instance = this
    }

    companion object {
        lateinit var selectedMovie: Movie
        /** application reference so leaking not an issue */
        @SuppressLint("StaticFieldLeak")
        lateinit var instance : Context

        val pseudoSearchVM by lazy {
            SearchVM()
        }

        val pseudoFavoriteVM by lazy {
            FavoriteVM()
        }

        val movieDb by lazy {
            Room.databaseBuilder(instance, MovieDb::class.java, "moviesdb").fallbackToDestructiveMigration().build()
        }
    }
}