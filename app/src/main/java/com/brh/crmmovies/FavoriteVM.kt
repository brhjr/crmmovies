package com.brh.crmmovies

import com.brh.crmmovies.model.FavoriteMovie
import com.brh.crmmovies.model.Movie
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FavoriteVM : BaseVM() {
    val favoriteDao = MovieApp.movieDb.favoriteDao()

    fun getAll(){
        requestState = RequestState.Loading()
        GlobalScope.launch {
            var movies = favoriteDao.getAll()
            requestState = RequestState.Success(movies)
        }
    }

    fun addToFavorties(movie : Movie) {
        GlobalScope.launch {
            favoriteDao.insert(FavoriteMovie(movie))
            getAll()
        }
    }

    fun removeFavorite(movie : FavoriteMovie) {
        GlobalScope.launch {
            favoriteDao.remove(movie)
            getAll()
        }
    }
}