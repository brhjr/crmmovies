package com.brh.crmmovies.model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Movie::class, FavoriteMovie::class), version = 2)
abstract class MovieDb : RoomDatabase() {
    abstract fun movieDao() : MovieDao
    abstract fun favoriteDao() : FavoriteMovieDao
}