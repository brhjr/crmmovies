package com.brh.crmmovies


import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.brh.crmmovies.model.FavoriteMovie
import com.brh.crmmovies.model.Movie
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_movie_item.view.*
import androidx.core.app.ActivityOptionsCompat



class MovieRecyclerViewAdapter(
    private var mMovies: List<Movie>
) : RecyclerView.Adapter<MovieRecyclerViewAdapter.ViewHolder>() {

    fun updateList(movies : List<Movie>) {
        mMovies = movies
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_movie_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie : Movie = mMovies[position]
        holder.tvTitle.text = movie.title
        holder.tvYear.text = movie.year
        println("poster " + movie.poster)
        Picasso.get()
            .load(movie.poster)
            .error(R.drawable.no_poster)
            .into(holder.mView.iv_poster)
        if (movie is FavoriteMovie)
            movie.favorited = true
        if (movie.favorited) {
            holder.ivFavorited.setImageResource(R.drawable.ic_favorited )
        } else {
            holder.ivFavorited.setImageResource(R.drawable.ic_not_favorite)
        }

        with(holder.mView) {
            tag = movie
            setOnClickListener{
                MovieApp.selectedMovie = movie
                val intent = Intent(context, MovieViewActivity::class.java)
                val options = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(context as Activity, holder.mView.iv_poster, "postert")
                context.startActivity(intent, options.toBundle())
            }
        }
        holder.ivFavorited.setOnClickListener{
            val iv = it as ImageView
            if (movie.favorited) {
                iv.setImageResource(R.drawable.ic_not_favorite )
                MovieApp.pseudoFavoriteVM.removeFavorite(FavoriteMovie(movie))
            } else {
                iv.setImageResource(R.drawable.ic_favorited)
                MovieApp.pseudoFavoriteVM.addToFavorties(movie)
            }
        }
    }

    override fun getItemCount(): Int = mMovies.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val tvTitle: TextView = mView.tv_title
        val tvYear: TextView = mView.tv_year
        val ivFavorited : ImageView = mView.iv_favorited
    }
}
