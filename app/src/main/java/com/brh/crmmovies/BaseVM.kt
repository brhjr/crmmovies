package com.brh.crmmovies

import kotlin.properties.Delegates
import kotlin.reflect.KProperty

open class BaseVM {
    var requestStateListener : RequestStateListener? = null
    /**
     * Use this as a LiveData imitation to let view know state has changed
     */
    var requestState : RequestState by Delegates.observable(
        initialValue = RequestState.Idle(),
        onChange = {
                _: KProperty<*>, _: RequestState?, newVal: RequestState ->
                requestStateListener?.onStateUpdate(newVal)
        }
    )
}